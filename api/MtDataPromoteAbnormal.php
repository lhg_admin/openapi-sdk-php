<?php

/**
 * Class MtDataPromoteAbnormal 异常订单数据接口
 * String mtAppKey required 美团appKey
 * String utmSource required 媒体code，对接时由美团侧生成
 * String queryType 订单查询时间区间类型，目前只支持账期时间，详见下方QueryTypeEnum
 * String startTime 订单查询开始时间（注: 配置queryType使用才会生效）
 * String endTime 订单查询结束时间（注: 配置queryType使用才会生效）
 * String page required 页数，从1开始，默认
 * String size required 每页数据，默认10
 */
class MtDataPromoteAbnormal extends DtkClient
{
    protected $mtAppKey;
    protected $utmSource;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/mt/data/promote/abnormal";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['mtAppKey','utmSource','queryType','startTime','endTime','page','size'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->mtAppKey) {
            return ['mtAppKey不能为空！', false];
        }
        if (!$this->utmSource) {
            return ['utmSource不能为空！', false];
        }
        return ['', true];
    }
}
