<?php

/**
 * Class MtPromotionLink 美团分销转链
 * String mtAppKey required 美团appKey
 * String utmSource required 媒体code，对接时由美团侧生成
 * String utmMedium required 二级渠道标识（原始值）
 * String activity required 活动ID (从分销联盟后台--【我要推广】中获取)
 * String pageLevel required 页面分销层级 (详见PageLevelEnum)
 * String promotionId required 推广位ID (在分销联盟后台创建生成)
 * String demandQrInfo 是否需要返回二维码，默认为false。一般在微信二级分销场景下需要，请渠道结合实际情况使用该字段，返回二维码可能会导致接口响应时间变长，取链失败等情况。
 */
class MtPromotionLink extends DtkClient
{
    protected $mtAppKey;
    protected $utmSource;
    protected $utmMedium;
    protected $activity;
    protected $pageLevel;
    protected $promotionId;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/mt/promotion/link";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['mtAppKey','utmSource','utmMedium','activity','pageLevel','promotionId','demandQrInfo'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->mtAppKey) {
            return ['mtAppKey不能为空！', false];
        }
        if (!$this->utmSource) {
            return ['utmSource不能为空！', false];
        }
        if (!$this->utmMedium) {
            return ['utmMedium不能为空！', false];
        }
        if (!$this->activity) {
            return ['activity不能为空！', false];
        }
        if (!$this->pageLevel) {
            return ['pageLevel不能为空！', false];
        }
        if (!$this->promotionId) {
            return ['promotionId不能为空！', false];
        }
        return ['', true];
    }
}
