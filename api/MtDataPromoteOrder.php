<?php

/**
 * Class MtDataPromoteOrder 美团分销订单查询
 * String orderType required 0cps订单，1CPA订单，默认0
 * String startVerifyDate 订单核验起始日期，支持日期与时间格式。日期：2019-05-08 时间：2019-05-08 00:00:00
 * String endVerifyDate 订单核验结束日期，支持日期与时间格式。日期：2019-05-08 时间：2019-05-08 23:59:59
 * String startAddDate 订单新增起始日期，仅支持日期格式。日期：2019-05-08
 * String endAddDate 订单新增结束日期，仅支持日期格式。 日期：2019-05-08
 * String startModifyDate 订单最后更新起始日期，支持日期与时间格式。日期：2019-05-08 时间：2019-05-08 00:00:00
 * String endModifyDate 订单最后更新结束日期，支持日期与时间格式。日期：2019-05-08 时间：2019-05-08 23:59:59
 * String uniqueOrderIds 唯子订单Id，每次查询限制100个，多个英文逗号分隔；CPA订单生效
 * String uniqueItemIds 唯一子订单Id，每次查询限制100个，多个英文逗号分隔；CPS订单才有
 * String settleTime 结算时间，时间格式：yyyy-MM-dd HH:mm:ss；CPS订单才有
 * String viewOrderIds C端展示订单Id,每次查询限制100个，多个英文逗号分隔
 * String orderItemTypes Integer类型集合,详情看下文OrderType，多个英文逗号分隔
 * String itemBizStatusList Integer类型集合,详情看下文ItemBizStatus，多个英文逗号分隔
 * String itemStatusList Integer类型集合,详情看下文ItemStatus，多个英文逗号分隔
 * String promotionId 推广位Id
 * String queryType 订单查询时间区间类型，详见下方QueryTypeEnum
 * String startTime 订单查询开始时间（注: 配置queryType使用才会生效）
 * String endTime 订单查询结束时间（注: 配置queryType使用才会生效）
 * String page 页数，从1开始，默认1
 * String size 每页数据，默认10
 */
class MtDataPromoteOrder extends DtkClient
{
    protected $orderType;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/mt/data/promote/order";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['orderType','startVerifyDate','endVerifyDate','startAddDate','endAddDate','startModifyDate','endModifyDate',
            'uniqueOrderIds','uniqueItemIds','settleTime','viewOrderIds','orderItemTypes','itemBizStatusList','itemStatusList',
            'promotionId','queryType','startTime','endTime','page','size'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->orderType) {
            return ['orderType不能为空！', false];
        }
        return ['', true];
    }
}
