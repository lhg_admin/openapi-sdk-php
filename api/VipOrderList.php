<?php

/**
 * Class VipOrderList 唯品会订单列表
 * String authId  授权id
 * String querySubsidyActFlag  是否查询补贴活动商品信息
 * String chanTag  pid
 * String vendorCode  vendorCode,工具商方式下会传入
 * String orderSnList  订单号列表：当传入订单号列表时，订单时间和更新时间区间可不传入
 * String updateTimeEnd  更新时间-起始 时间戳 单位毫秒
 * String updateTimeStart  更新时间-起始 时间戳 单位毫秒
 * String pageSize  页面大小：默认20
 * Integer page required 页码：从1开始
 * String orderTimeEnd 订单时间结束 时间戳 单位毫秒
 * String orderTimeStart 订单时间起始 时间戳 单位毫秒
 * String status 订单状态:0-不合格，1-待定，2-已完结，该参数不设置默认代表全部状态
 */
class VipOrderList extends DtkClient
{
    protected $page;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/vip/order-list";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['authId','querySubsidyActFlag','chanTag','vendorCode','orderSnList','updateTimeEnd','updateTimeStart','pageSize',
            'page','orderTimeEnd','orderTimeStart','status'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->page) {
            return ['page不能为空！', false];
        }
        return ['', true];
    }
}
