<?php

/**
 * Class GetPddOrderSearch 拼多多订单查询
 * Integer queryOrderType 订单类型：1-推广订单；2-直播间订单
 * String startUpdateTime 查询开始时间，和开始时间相差不能超过24小时。
 * String endUpdateTime 查询结束时间，和开始时间相差不能超过24小时。
 * Integer pageId 页码，默认1，注：使用最后更新时间范围增量同步时，必须采用倒序的分页方式（从最后一页往回取）才能避免漏单问题。
 * Integer pageSize 返回的每页结果订单数，默认为100，范围为10到100，建议使用40~50，可以提高成功率，减少超时数量。
 * String pids 拼多多联盟pid，多个使用英文逗号分隔符，用于匹配拼多多推广订单(如果没有绑定pid，则必填)
 */
class GetPddOrderSearch extends DtkClient
{
    protected $queryOrderType;
    protected $startUpdateTime;
    protected $endUpdateTime;
    protected $pageId;
    protected $pageSize;
    protected $pids;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/api/dels/pdd/order/incrementSearch";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return [
            'queryOrderType','startUpdateTime','endUpdateTime','pageId','pageSize','pids'];
    }

    /**
     * @return array
     */
    public function check()
    {
        return ['', true];
    }
}
