<?php

/**
 * Class VipGoodsDetail 唯品会商品详情
 * Array goodsIdList required 商品id列表
 * String chanTag  自定义渠道标识,同推广位, pid
 * String authId  唯品会授权id
 */
class VipGoodsDetail extends DtkClient
{
    protected $goodsIdList;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/vip/goods-detail";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['goodsIdList', 'chanTag','authId'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->goodsIdList) {
            return ['goodsIdList不能为空！', false];
        }
        return ['', true];
    }
}
