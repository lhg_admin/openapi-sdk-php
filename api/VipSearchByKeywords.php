<?php

/**
 * Class VipSearchByKeywords 唯品会联盟搜索
 * String keyword required 关键词
 * String fieldName 排序字段
 * Integer order 排序顺序：0-正序，1-逆序，默认正序
 * Integer page required 页码
 * String pageSize 页面大小：默认20,最大50
 * String priceStart 价格区间---start
 * String priceEnd 价格区间---end
 * String queryReputation 是否查询商品评价信息:默认不查询，该数据在详情页有返回，没有特殊需求，建议不查询，影响接口性能
 * String queryStoreServiceCapability 是否查询店铺服务能力信息：默认不查询，该数据在详情页有返回，没有特殊需求，建议不查询，影响接口性能
 * String queryStock 是否查询库存:默认不查询
 * String queryActivity 是否查询商品活动信息:默认不查询
 * String queryPrepay 是否查询商品预付信息:默认不查询
 * String commonParams 通用参数
 * String vendorCode 工具商code
 * String chanTag 用户pid
 * String queryExclusiveCoupon 是否查询专属红包信息：默认不查询
 * String queryCpsInfo 是否返回cps链接：0-不查询，1-tra_from参数,2-小程序链接，默认为0，查询多个时按照位运算处理，例如：3表示查询tra_from参数+小程序链接
 * String mobile 买家手机号
 * String research 搜索无结果时是否返回推荐词搜索结果,默认是
 * String queryFuturePrice 是否查询未来价信息：默认不查询
 * String authId 授权id
 */
class VipSearchByKeywords extends DtkClient
{
    protected $page;
    protected $keyword;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/vip/search-by-keywords";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['keyword', 'fieldName','order','page','pageSize','priceStart','priceEnd','queryReputation','queryStoreServiceCapability',
            'queryStock','queryActivity','queryPrepay','commonParams','vendorCode','chanTag','queryExclusiveCoupon','queryCpsInfo',
            'mobile','research','queryFuturePrice','authId'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->keyword) {
            return ['keyword不能为空！', false];
        }
        if (!$this->page) {
            return ['page不能为空！', false];
        }
        return ['', true];
    }
}
