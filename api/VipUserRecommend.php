<?php

/**
 * Class VipUserRecommend 唯品会猜你喜欢
 * String pageSize  页面大小：默认20
 * Integer page required 页码：从1开始
 * String inStock 是否有货 1:有货 0:无货 默认1
 * String goodsSaleStats 商品售卖状态 1（在售）， 2（预热）， 3（在售+预热） 默认1
 * String offlineStore 筛选线下店商品：1只返线下店，0或者不传只返回特卖会
 * String commonParams 通用参数：能获取到则须传入
 * String chanTag pid
 * String queryCpsInfo 是否返回cps链接：0-不查询，1-tra_from参数,2-小程序链接，默认为0，查询多个时按照位运算处理，例如：3表示查询tra_from参数+小程序链接
 * String queryFuturePrice 是否查询未来价信息：默认不查询
 */
class VipUserRecommend extends DtkClient
{
    protected $page;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/vip/user-recommend";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['pageSize', 'page','inStock','goodsSaleStats','offlineStore','commonParams','chanTag','queryCpsInfo','queryFuturePrice'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->page) {
            return ['page不能为空！', false];
        }
        return ['', true];
    }
}
