<?php

/**
 * Class ClipboardContentParse 剪切板内容解析识别
 * 应用场景：从其他渠道获取到的热门商品/活动的推广内容，如果内容中包含淘口令或者推广链接，则可以在打开app时调用此接口解析出商品的基本信息（佣金，价格，商品ID……）或者是活动链接，并同时进行转链
 * 接口说明：可在解析出淘宝、京东、拼多多的商品和活动会场的链接或口令的同时，同步进行转链
 */
class ClipboardContentParse extends DtkClient
{

    protected $methodType = 'GET';
    protected $requestParams = [];

    /**
     * @var string content，文本内容
     */
    protected $content;

    /**
     * @var String 淘宝联盟pid
     */
    protected $TbPid;

    /**
     * @var String 淘宝联盟渠道id
     */
    protected $TbChannelId;

    /**
     * @var integer 京东联盟unionId
     */
    protected $JdUnionId;

    /**
     * @var integer 京东联盟pid
     */
    protected $JdPid;

    /**
     * @var String 京东推广位id
     */
    protected $jdPositionId;

    /**
     * @var String 自定义参数，为链接打上自定义标签；自定义参数最长限制64个字节；格式为： {"uid":"11111","sid":"22222"} 其中 uid 用户唯一标识，可自行加密后传入，每个用户仅且对应一个标识，必填； sid 上下文信息标识，例如sessionId等，非必填。该json字符串中也可以加入其他自定义的key。（如果使用GET请求，请使用URLEncode处理参数）
     */
    protected $customerParameters;

    /**
     * @var Number 平台的淘宝授权id（获取地址：https://www.dataoke.com/shouquan?type=1&auth_id=1），如果传入了该参数则必须填写对应淘宝联盟授权账号的pid（2/24新增字段）
     */
    protected $tbAuthId;

    /**
     * @var Number 平台的京东授权id（获取地址：https://www.dataoke.com/shouquan?type=2），如果传入了该参数则必须填写对应京东联盟授权账号的pid（2/24新增字段）
     */
    protected $jdAuthId;

    /**
     * @var Number 平台的拼多多授权id（获取地址：https://www.dataoke.com/shouquan?type=3），如果传入了该参数则必须填写对应多多进宝授权账号的pid（2/24新增字段）
     */
    protected $pddAuthId;

    /**
     * @var String 拼多多联盟pid
     */
    protected $PddPid;

    const METHOD = '/api/dels/kit/contentParse';

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['content', 'TbPid', 'TbChannelId', 'JdUnionId', 'JdPid', 'jdPositionId', 'PddPid', 'customerParameters', 'tbAuthId', 'jdAuthId', 'pddAuthId'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->content) {
            return ['content不能为空！', false];
        }
        return ['', true];
    }
}