<?php

/**
 * Class GoodsListV2WithOauth 唯品会商品列表
 * String authId  授权id
 * String channelType required 频道类型:0-超高佣，1-出单爆款; 当请求类型为频道时必传
 * String queryReputation  是否查询商品评价信息:默认不查询，该数据在详情页有返回，没有特殊需求，建议不查询
 * String queryStoreServiceCapability  是否查询店铺服务能力信息:默认不查询，该数据在详情页有返回，没有特殊需求，建议不查询
 * String sourceType required 请求源类型：0-频道，1-组货
 * String jxCode  精选组货码：当请求源类型为组货时必传
 * String queryStock  是否查询库存:默认不查询
 * String queryActivity  是否查询商品活动信息:默认不查询
 * String queryPrepay  是否查询商品预付信息:默认不查询
 * String plat  用户平台：1-PC,2-APP,3-小程序,不传默认为APP
 * String deviceType  设备号类型：IMEI，IDFA，OAID，有则传入
 * String deviceValue  设备号MD5加密后的值，有则传入,IDFA大写后md5，IMER小写后md5，OAID原文md5
 * String ip  用户ip地址
 * String longitude  经度 如:29.590961456298828
 * String latitude  纬度 如:106.51573181152344
 * String warehouse  分仓 VIP_NH：广州仓， VIP_SH：上海仓， VIP_BJ：北京仓， VIP_CD：成都仓， VIP_HZ：华中仓， ALL：全国仓
 * String chanTag  pid
 * String queryExclusiveCoupon  是否查询专属红包信息：默认不查询
 * String queryCpsInfo  是否返回cps链接：0-不查询，1-tra_from参数,2-小程序链接，默认为0，查询多个时按照位运算处理，例如：3表示查询tra_from参数+小程序链接
 * String queryFuturePrice  是否查询未来价信息：默认不查询
 * Integer pageSize required 分页大小:默认20，最大50
 * String offset  查询偏移(必传字段)，查询第一页传0，后续查询传上一页返回的nextPageOffset字段
 */
class GoodsListV2WithOauth extends DtkClient
{
    protected $channelType;
    protected $sourceType;
    protected $pageSize;

    protected $methodType = 'GET';
    protected $requestParams = [];

    const METHOD = "/open-api/vip/goods-list-v2-with-oauth";

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD;
    }

    /**
     * 可用参数
     * @return string[]
     */
    public function getParamsField()
    {
        return ['authId','channelType','queryReputation','queryStoreServiceCapability','sourceType','jxCode','queryStock',
            'queryActivity','queryPrepay','plat','deviceType','deviceValue','ip','longitude','latitude','warehouse','chanTag',
            'queryExclusiveCoupon','queryCpsInfo','queryFuturePrice','pageSize','offset'];
    }

    /**
     * @return array
     */
    public function check()
    {
        if (!$this->sourceType && $this->sourceType != 0) {
            return ['sourceType不能为空！', false];
        }
        return ['', true];
    }
}
